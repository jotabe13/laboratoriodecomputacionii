package Desafios;

import java.util.Random;
import java.util.Scanner;

public class Desafio10 {
    public static void main(String[] args) {
        int usuario,random,intentos=0;
        boolean bandera=true;
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        random=rand.nextInt(100);
        while(bandera){
            intentos++;
            System.out.println("Ingrese el numero que usted cree que es el correcto: ");
            usuario=sc.nextInt();
            if(usuario<random){
                System.out.println("El numero que ingreso es menor, siga intentando");
            } else if(usuario>random){
                System.out.println("El numero que ingreso es mayor, siga intentando");
            } else {
                System.out.println("Correcto! El numero de intentos fue: "+intentos);
                bandera=false;
            }
        }


    }

}
