package Desafios;

import java.util.Scanner;

public class Desafio9 {
    public static void main(String[] args) {
        char genero;
        boolean bandera=true;
        int altura, resultado;
        Scanner sc = new Scanner(System.in);
        while (bandera) {
            System.out.println("Ingrese su altura en cm: ");
            altura = sc.nextInt();
            System.out.println("Ingrese su genero: ");
            genero = sc.next().charAt(0);
            if (genero == 'm' || genero=='M') {
                resultado = altura - 110;
                System.out.println("Su peso ideal es de " + resultado);
                bandera=false;
            } else if (genero == 'F' || genero == 'f') {
                resultado = altura - 120;
                System.out.println("Su peso ideal es de " + resultado);
                bandera=false;
            } else System.out.println("El dato ingresado es erroneo, pruebe de nuevo");
        }
    }
}
