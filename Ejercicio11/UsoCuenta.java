package Desafios;



public class UsoCuenta extends CuentaCorriente{
     static CuentaCorriente Cuenta1 = new CuentaCorriente(10000.1,"Juanma Macotena");
     static CuentaCorriente Cuenta2= new CuentaCorriente(9000.20,"Pablo Sanchez");

    public UsoCuenta(String nombreTitular) {
        super(nombreTitular);
    }

    public UsoCuenta(double saldo, String nombreTitular) {
        super(saldo, nombreTitular);
    }

    public static void main(String[] args) {
        System.out.println(Cuenta1.getNumeroCuenta());
        transferencia(Cuenta1,Cuenta2);
    }

}
