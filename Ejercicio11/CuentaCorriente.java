package Desafios;

import java.util.Random;
import java.util.Scanner;

public class CuentaCorriente {

    private double saldo=999.9;
    private String nombreTitular="Juan Burich";
    Random random = new Random();
    private long numeroCuenta=random.nextLong();

    public CuentaCorriente(String nombreTitular){
        this.saldo = 0;
        this.nombreTitular = nombreTitular;
        if (numeroCuenta<0){
            numeroCuenta=numeroCuenta*(-1);
        }
    }
    public CuentaCorriente(double saldo, String nombreTitular) {
        this.saldo = saldo;
        this.nombreTitular = nombreTitular;
        if (numeroCuenta<0){
            numeroCuenta=numeroCuenta*(-1);
        }
    }

    public double getSaldo() {
        return saldo;
    }

    public long getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }


    public void ingresarDinero(int dinero){
        if (dinero>0){
            this.saldo+=dinero;
        } else System.out.println("No se pudo ingresar el dinero.");
    }
    public void sacarDinero(int dinero){
        if(this.saldo<dinero){
            System.out.println("Operacion no completada. Usted esta queriendo retirar mas dinero del que posee en la cuenta");
        } else this.saldo-=dinero;
    }


    public static void transferencia(CuentaCorriente Cuenta1, CuentaCorriente Cuenta2){
        double importe;
        System.out.println("Ingrese el importe que desea para la transferencia");
        Scanner sc = new Scanner(System.in);
        importe=sc.nextDouble();
        Cuenta1.setSaldo(Cuenta1.getSaldo()-importe);
        Cuenta2.setSaldo(Cuenta2.getSaldo()+importe);
        System.out.println("Cuenta 1: "+ "\n" + Cuenta1.getNombreTitular()+"\n"+Cuenta1.getSaldo());
        System.out.println("Cuenta 2: "+ "\n" + Cuenta2.getNombreTitular()+"\n"+Cuenta2.getSaldo());
    }


}
